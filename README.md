## Purpose
It is a small technical exercise pairing Reactive Streams of Spring 5 with Angular 6 and MongoDB.

The idea is to upload an JSON file with an array of URLs and process their content in reactive manner, saving and streaming end-result at the end.

## Server

Service is a classical Spring Boot Application.

For a web run `ng serve` for a dev server and navigate to `http://localhost:4200/`

## Status

The code is in verly early stage now, not really reactive yet and with no tests whatsoever.