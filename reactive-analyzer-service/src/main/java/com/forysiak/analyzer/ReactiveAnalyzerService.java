package com.forysiak.analyzer;

import com.forysiak.analyzer.configs.AppConfig;
import com.forysiak.analyzer.domain.candidate.dao.CandidateDao;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import reactor.ipc.netty.NettyContext;

public class ReactiveAnalyzerService {
  public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    CandidateDao candidateDao = context.getBean(CandidateDao.class);
    candidateDao.dropCollection();
    candidateDao.insertBasicMockCandidates();

    context.getBean(NettyContext.class).onClose().block();
		SpringApplication.run(ReactiveAnalyzerService.class, args);
	}
}
