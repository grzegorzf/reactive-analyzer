package com.forysiak.analyzer.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({MongoConfig.class, WebConfig.class})
@ComponentScan(basePackages = "com.forysiak.analyzer")
public class AppConfig {

}

