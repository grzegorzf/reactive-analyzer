package com.forysiak.analyzer.domain.candidate.model;

import lombok.Getter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.Clock;
import java.time.LocalDateTime;

@ToString
@Getter
@Document(collection = "Candidate")
public class CandidateDocument {

  public CandidateDocument(String url, long rank, Qualification qualification) {
    this.url = url;
    this.rank = rank;
    this.qualification = qualification;
    this.evaluatedAt = LocalDateTime.now(Clock.systemUTC());
  }

  @Id
  private String id;

  @Indexed
  private String url;

  private long rank;

  private Qualification qualification;

  private boolean customer = false;


  private LocalDateTime evaluatedAt;

  public void changeQualificationAndRank(Qualification qualification, long rank){
    this.qualification = qualification;
    this.rank = rank;
    this.evaluatedAt = LocalDateTime.now(Clock.systemUTC());
  }

}
