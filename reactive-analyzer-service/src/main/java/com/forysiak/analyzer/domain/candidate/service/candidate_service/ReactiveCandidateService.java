package com.forysiak.analyzer.domain.candidate.service.candidate_service;

import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.analyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.analyzer.domain.candidate.model.Qualification;
import com.forysiak.analyzer.domain.candidate.predicate.DistinctByKey;
import com.forysiak.analyzer.domain.candidate.predicate.HasCandidateEvaluationExpired;
import com.forysiak.analyzer.domain.candidate.predicate.HasValidURL;
import com.forysiak.analyzer.domain.candidate.repository.CandidateRepository;
import com.forysiak.analyzer.domain.candidate.service.crawler_service.CrawlerService;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import static java.util.Objects.isNull;

@AllArgsConstructor
@Service
public class ReactiveCandidateService implements CandidateService {

  private final CandidateRepository candidateRepository;
  private final CrawlerService crawlerService;
  private final HasValidURL hasValidURL;
  private final DistinctByKey distinctByKey;
  private final HasCandidateEvaluationExpired hasCandidateEvaluationExpired;

  @Override
  public Flux<CandidateDocument> evaluateCandidates(Flux<CandidateProposition> candidatePropositions) {
    return candidatePropositions
        .log()
        .filter(hasValidURL)
        .filter(distinctByKey.test(CandidateProposition::getUrl))
        .flatMap(proposition -> evaluateCandidate(proposition).subscribeOn(Schedulers.parallel()), 10);
  }

  @Override
  public Mono<CandidateDocument> reevaluateCandidate(String candidateId) {
    return candidateRepository.findById(candidateId)
        .flatMap(candidateDocument -> this.evaluateCandidate(new CandidateProposition(candidateDocument.getUrl(), candidateDocument.getRank())));
  }

  @Override
  public Mono<CandidateDocument> evaluateCandidate(CandidateProposition proposition) {
    return
        candidateRepository.findByUrl(proposition.getUrl())
        .switchIfEmpty(persistProposition(proposition))
        .flatMap(candidate -> updateCandidateIfNecessary(proposition, candidate));
  }

  @Override
  public Mono<CandidateDocument> updateCandidateIfNecessary(CandidateProposition proposition, CandidateDocument candidateDocument) {
    if (hasCandidateEvaluationExpired.test(candidateDocument)){
      Qualification qualification = crawlerService.isPropositionMarfeelizable(proposition);
        candidateDocument.changeQualificationAndRank(qualification, proposition.getRank());
      return candidateRepository.save(candidateDocument);
    }
    return Mono.just(candidateDocument);
  }

  @Override
  public Mono<CandidateDocument> persistProposition(CandidateProposition proposition) {
    Qualification qualification = crawlerService.isPropositionMarfeelizable(proposition);
    CandidateDocument candidate = new CandidateDocument(proposition.getUrl(), proposition.getRank(), qualification);
    return candidateRepository.save(candidate);
  }

  @Override
  public Flux<CandidateDocument> streamAllCandidates() {
    return candidateRepository.findAll();
  }

}
