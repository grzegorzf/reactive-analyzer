package com.forysiak.analyzer.domain.candidate.model;

import java.io.Serializable;

public enum EvaluationResult implements Serializable {
  QUALIFIES,
  NOT_QUALIFIES,
  INVALID
}
