package com.forysiak.analyzer.domain.candidate.service.crawler_service;

import com.forysiak.analyzer.domain.candidate.function.UrlToFullUrl;
import com.forysiak.analyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.analyzer.domain.candidate.model.Qualification;
import com.forysiak.analyzer.domain.candidate.predicate.IsDocumentMarfeelizable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.forysiak.analyzer.domain.candidate.model.Qualification.INVALID;
import static com.forysiak.analyzer.domain.candidate.model.Qualification.NEGATIVE;
import static com.forysiak.analyzer.domain.candidate.model.Qualification.POSITIVE;

@AllArgsConstructor
@Slf4j
@Service
public class ReactiveCrawlerService implements CrawlerService {

  private final IsDocumentMarfeelizable isDocumentMarfeelizable;
  private final UrlToFullUrl urlToFullUrl;

  @Override
  public Qualification isPropositionMarfeelizable(CandidateProposition candidateProposition) {
    String fullUrl = urlToFullUrl.apply(candidateProposition.getUrl());
    try {
      Document document = getDocument(fullUrl);
      return isDocumentMarfeelizable.test(document) ? POSITIVE: NEGATIVE;
    } catch (IOException e) {
      log.error("isPropositionMarfeelizable failed for url: {}, rank {}", candidateProposition.getUrl(), candidateProposition.getRank());
      return INVALID;
    }
  }

  @Override
  public Document getDocument(String fullUrl) throws IOException {
    return Jsoup.connect(fullUrl).get();
  }
}


