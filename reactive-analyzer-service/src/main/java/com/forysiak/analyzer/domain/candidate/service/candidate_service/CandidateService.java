package com.forysiak.analyzer.domain.candidate.service.candidate_service;

import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.analyzer.domain.candidate.model.CandidateProposition;
import org.bson.types.ObjectId;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CandidateService {

  Flux<CandidateDocument> evaluateCandidates(Flux<CandidateProposition> candidatePropositions);

  Mono<CandidateDocument> reevaluateCandidate(String candidateId);

  Mono<CandidateDocument> evaluateCandidate(CandidateProposition candidateProposition);

  Mono<CandidateDocument> updateCandidateIfNecessary(CandidateProposition candidateProposition, CandidateDocument candidateDocument);

  Mono<CandidateDocument> persistProposition(CandidateProposition candidateProposition);

  Flux<CandidateDocument> streamAllCandidates();

}
