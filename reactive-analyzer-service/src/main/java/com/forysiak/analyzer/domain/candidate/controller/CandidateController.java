package com.forysiak.analyzer.domain.candidate.controller;

import com.forysiak.analyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.analyzer.domain.candidate.repository.CandidateRepository;
import com.forysiak.analyzer.domain.candidate.service.candidate_service.CandidateService;
import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(CandidateApi.BASE_API_PATH)
@AllArgsConstructor
@Slf4j
public class CandidateController {

  private final CandidateService candidateService;
  private final CandidateRepository candidateRepository;

  @PostMapping(CandidateApi.BASE_CANDIDATE_PATH)
  public Flux<CandidateDocument> evaluateCandidates(@Valid @RequestBody Flux<CandidateProposition> candidatePropositions) {
    log.info("Started evaluation of candidatePropositions");
    return candidateService.evaluateCandidates(candidatePropositions);
  }

  @GetMapping(CandidateApi.BASE_CANDIDATE_PATH)
  public Flux<CandidateDocument> getAllCandidates() {
    Flux<CandidateDocument> candidatesPublisher = candidateRepository.findAll();
    log.info("Started publishing of all candidates", candidatesPublisher);
    return candidatesPublisher;
  }

  @PutMapping(CandidateApi.REEVALUATE_CANDIDATE_PATH )
  public Mono<CandidateDocument> reevaluateCandidate(@PathVariable String candidateId) {
    log.info("Started re-evaluation for candidateId: {}", candidateId);
    return candidateService.reevaluateCandidate(candidateId);
  }

}
