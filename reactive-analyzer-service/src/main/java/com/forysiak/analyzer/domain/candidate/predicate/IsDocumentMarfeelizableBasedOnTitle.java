package com.forysiak.analyzer.domain.candidate.predicate;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class IsDocumentMarfeelizableBasedOnTitle implements IsDocumentMarfeelizable {

  private static final  String REQUESTED_HTML_ELEMENT = "title";
  private static final String REQUESTED_CONTAINED_TEXT_VALUE = "\\*news\\*noticas";

  @Override
  public boolean test(Document document) {
    return document
        .select(REQUESTED_HTML_ELEMENT)
        .text()
        .matches(REQUESTED_CONTAINED_TEXT_VALUE);
  }
}
