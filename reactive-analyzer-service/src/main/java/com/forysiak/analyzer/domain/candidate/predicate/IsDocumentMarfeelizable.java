package com.forysiak.analyzer.domain.candidate.predicate;

import org.jsoup.nodes.Document;

import java.util.function.Predicate;

public interface IsDocumentMarfeelizable extends Predicate<Document> {
}
