package com.forysiak.analyzer.domain.candidate.controller;

public interface CandidateApi {
  String BASE_API_PATH = "/api";
  String BASE_CANDIDATE_PATH = "/candidates";
  String VALID_CANDIDATES_PATH = "/valid-candidates";
  String REEVALUATE_CANDIDATE_PATH = BASE_CANDIDATE_PATH + "/{*candidateId}";
}
