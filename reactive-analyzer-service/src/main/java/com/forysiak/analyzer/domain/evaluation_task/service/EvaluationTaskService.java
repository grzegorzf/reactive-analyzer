package com.forysiak.analyzer.domain.evaluation_task.service;

import com.forysiak.analyzer.domain.evaluation_task.model.EvaluationTaskDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface EvaluationTaskService extends ReactiveCrudRepository<EvaluationTaskDocument, String> {

}
