package com.forysiak.analyzer.domain.candidate.dao;

import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.analyzer.domain.candidate.model.Qualification;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CandidateDao {
  private final ReactiveMongoTemplate mongo;

  private static final String COLLECTION_NAME = "Candidate";

  public void dropCollection(){
    mongo.dropCollection(CandidateDocument.class).subscribe();
  }

  public void insertBasicMockCandidates(){
    insertCandidate("forysiak.com", 1, Qualification.POSITIVE);
    insertCandidate("smartlope.com", 2, Qualification.POSITIVE);
    insertCandidate("wpl.pl", 2, Qualification.NEGATIVE);
  }

  private void insertCandidate(String url, long rank, Qualification qualification){
    Query query = new Query();
    query.addCriteria(Criteria.where("url").is(url));
    mongo.find(query, CandidateDocument.class).switchIfEmpty(
        candidate -> {
          CandidateDocument candidateDocument = new CandidateDocument(url, rank, qualification);
          mongo.insert(candidateDocument, COLLECTION_NAME).subscribe();
        }
    ).subscribe();
  }

}
