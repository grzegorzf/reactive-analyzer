package com.forysiak.analyzer.domain.candidate.predicate;

import com.forysiak.analyzer.domain.candidate.model.CandidateProposition;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;
import java.util.regex.Pattern;

@Component
public class HasValidURL implements Predicate<CandidateProposition> {

  private static final Pattern VALID_URL_REGEX  = Pattern.compile("^(?:https?:\\/\\/)?(?:www\\.)?[a-zA-Z0-9./]+$");

  @Override
  public boolean test(CandidateProposition candidateProposition) {
    return VALID_URL_REGEX.matcher(candidateProposition.getUrl()).find();
  }
}
