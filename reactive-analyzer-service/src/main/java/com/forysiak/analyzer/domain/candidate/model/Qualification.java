package com.forysiak.analyzer.domain.candidate.model;

public enum Qualification {
  POSITIVE,
  NEGATIVE,
  INVALID
}
