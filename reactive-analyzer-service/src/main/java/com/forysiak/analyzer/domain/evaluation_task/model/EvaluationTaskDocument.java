package com.forysiak.analyzer.domain.evaluation_task.model;

import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Document(collection = "EvaluationTask")
public class EvaluationTaskDocument {

  @Id
  private ObjectId id = new ObjectId();

  private LocalDate evaluatedAt;

  @DBRef
  private List<CandidateDocument> positiveCandidates;

  @DBRef
  private List<CandidateDocument> negativeCandidates;

  @DBRef
  private List<CandidateDocument> invalidCandidates;

  private long processingTime;

  @Transient
  public int getSumOfAllCandidates(){
    return Stream.of(positiveCandidates,negativeCandidates,invalidCandidates
    ).flatMap(Collection::stream).collect(Collectors.toList()).size();
  }
}
