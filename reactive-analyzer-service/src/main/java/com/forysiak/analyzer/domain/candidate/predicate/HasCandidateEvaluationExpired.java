package com.forysiak.analyzer.domain.candidate.predicate;

import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Predicate;

@Component
public class HasCandidateEvaluationExpired implements Predicate<CandidateDocument> {

  private static final int EXPIRATION_TIME_IN_WEEKS = 2;

  @Override
  public boolean test(CandidateDocument candidateDocument) {
    LocalDateTime expirationDate = LocalDate.now().minusWeeks(EXPIRATION_TIME_IN_WEEKS).atStartOfDay();
    return candidateDocument.getEvaluatedAt().isAfter(expirationDate);
  }
}
