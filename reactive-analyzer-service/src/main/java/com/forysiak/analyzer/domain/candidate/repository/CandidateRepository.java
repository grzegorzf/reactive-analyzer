package com.forysiak.analyzer.domain.candidate.repository;

import com.forysiak.analyzer.domain.candidate.model.CandidateDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface CandidateRepository extends ReactiveCrudRepository<CandidateDocument, String> {
  Mono<CandidateDocument> findByUrl(String url);
}
