package com.forysiak.analyzer.domain.candidate.function;

import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.regex.Pattern;

@Component
public class UrlToFullUrl implements Function<String, String> {

  private static final String HTTPS = "https://";
  private static final Pattern PROTOCOL_PREFIX  = Pattern.compile("^" + HTTPS);

  @Override
  public String apply(String url) {
    boolean hasProtocolPrefix = PROTOCOL_PREFIX.matcher(url).find();
    if (!hasProtocolPrefix) url = HTTPS + url;

    return url;
  }
}

