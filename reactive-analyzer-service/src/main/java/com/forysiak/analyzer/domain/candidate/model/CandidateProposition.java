package com.forysiak.analyzer.domain.candidate.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class CandidateProposition implements Serializable {

  @NotNull
  private String url;

  @NotNull
  private long rank;

}
