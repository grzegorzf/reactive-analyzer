package com.forysiak.analyzer.domain.candidate.service.crawler_service;

import com.forysiak.analyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.analyzer.domain.candidate.model.Qualification;
import org.jsoup.nodes.Document;

import java.io.IOException;

public interface CrawlerService {

  Qualification isPropositionMarfeelizable(CandidateProposition candidateProposition);

  Document getDocument(String fullUrl) throws IOException;

}
