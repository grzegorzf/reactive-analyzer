package com.forysiak.analyzer.domain.candidate.service;

import com.forysiak.analyzer.ReactiveAnalyzerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReactiveAnalyzerService.class)
public class CandidateServiceTest {

  @Test
  public void should_properly_check_evaluation_expiration(){
    // TODO
  }

  @Test
  public void should_properly_accept_or_reject_proposition_basing_on_url(){
    // TODO
  }

  @Test
  public void should_properly_evaluate_update_persist_or_ignore(){
    // TODO
  }

}
