package com.forysiak.analyzer.domain.candidate.controller;

import com.forysiak.analyzer.ReactiveAnalyzerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReactiveAnalyzerService.class)
public class CandidateControllerTest {

  @Test
  public void should_properly_invoke_evaluation(){
    // TODO
  }

  @Test
  public void should_properly_invoke_reevaluation(){
    // TODO
  }

  @Test
  public void should_properly_invoke_find_all_stream(){
    // TODO
  }

}
