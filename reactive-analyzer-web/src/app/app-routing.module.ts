import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TasksPageComponent} from "./tasks";
import {CandidatesPageComponent} from "./candidates";

const routes: Routes = [
  {path: "candidates", component: CandidatesPageComponent},
  {path: "tasks", component: TasksPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
