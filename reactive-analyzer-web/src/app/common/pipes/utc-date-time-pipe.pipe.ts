import { Pipe, PipeTransform } from '@angular/core';
/*
 TODO -> its a temporal solution just for the sake of writing custom pipe -> local date time proper format need to be managed by backend
*/
@Pipe({name: 'utcDateTime'})
export class UtcDateTimePipe implements PipeTransform {
  transform(arrayDate: number[]): Date {
    if (!arrayDate.length) return new Date();
    arrayDate.pop();
    --arrayDate[1];
    return new Date(...arrayDate);
  }
}
