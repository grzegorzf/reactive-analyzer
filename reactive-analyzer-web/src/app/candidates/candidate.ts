export interface Qualification {
  POSITIVE,
  NEGATIVE,
  INVALID
}

export class Candidate {
  id: string;
  url: string;
  rank: number;
  qualification: Qualification;
  evaluatedAt: Date;
}
