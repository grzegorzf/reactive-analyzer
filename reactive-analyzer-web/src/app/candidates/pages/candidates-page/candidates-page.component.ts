import {Component, OnInit, ViewChild} from '@angular/core';
import {CandidatesService} from "../../candidates.service";
import {Observable} from "rxjs/internal/Observable";
import {MatPaginator, MatTableDataSource, MatSort} from "@angular/material";
import {Candidate} from "../../candidate";

@Component({
  selector: 'app-candidates-pages',
  templateUrl: './candidates-page.component.html',
  styleUrls: ['./candidates-page.component.scss']
})

export class CandidatesPageComponent implements OnInit {

  constructor(private data: CandidatesService) { }

  dataSource = new MatTableDataSource<Candidate>([]);
  columnsToDisplay = ['url', 'qualification', 'rank', 'evaluatedAt', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  onReevaluate(candidateId) {
    this.data.reevaluateCandidate(candidateId).subscribe(data => console.log("TODO::handler",  data));
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.data.getCandidates().subscribe(
      (data) => {
        this.dataSource.data = data
      }
    )
  }

}
