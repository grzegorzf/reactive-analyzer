import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";

@Injectable({
  providedIn: 'root'
})
export class CandidatesService {

  constructor(private http: HttpClient) { }

  getCandidates(): Observable<any>{
    return this.http.get("http://localhost:8282/api/candidates")
  }

  reevaluateCandidate({id}): Observable<any>{
    return this.http.put(`http://localhost:8282/api/candidates/${id}`, {})
  }
}
