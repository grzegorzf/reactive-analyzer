import { NgModule } from '@angular/core';
import { CandidatesPageComponent } from './pages';

@NgModule({
  declarations: [
    CandidatesPageComponent
  ],
  imports: [],
  providers: []
})
export class CandidatesModule { }
