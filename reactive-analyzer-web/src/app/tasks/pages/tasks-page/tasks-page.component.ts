import { Component, OnInit } from '@angular/core';
import {TasksService} from "../../tasks.service";
import {Observable} from "rxjs/internal/Observable";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks-page.component.html',
  styleUrls: ['./tasks-page.component.scss']
})
export class TasksPageComponent implements OnInit {

  constructor(private data: TasksService) { }

  ngOnInit() {
  }

}
