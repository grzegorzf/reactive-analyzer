
export class Task {
  evaluatedAt: Date;
  positiveCandidates: number;
  negativeCandidates: number;
  invalidCandidates: number;
  requestPayload: any
}
