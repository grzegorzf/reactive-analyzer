import { NgModule } from '@angular/core';
import { TasksPageComponent } from './pages';

@NgModule({
  declarations: [
    TasksPageComponent
  ],
  imports: [],
  providers: []
})
export class TasksModule { }
