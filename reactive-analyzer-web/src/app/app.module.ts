import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/app/app.component';
import { NavigationComponent } from './ui/navigation/navigation.component';
import { MaterialModule } from './ui/material.module';
import { CandidatesPageComponent } from './candidates';
import {TasksPageComponent} from "./tasks/pages/tasks-page";
import {HttpClientModule} from "@angular/common/http";
import {UtcDateTimePipe} from "./common/pipes/utc-date-time-pipe.pipe";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
// import {TasksModule} from "./tasks/tasks.module";  TODO -> to modularize Tasks
// import {CandidatesModule} from "./candidates/candidates.module"; TODO -> to modularize Candidates

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    CandidatesPageComponent,
    TasksPageComponent,
    UtcDateTimePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
